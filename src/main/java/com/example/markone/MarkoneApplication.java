package com.example.markone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarkoneApplication {

	public static void main(String[] args) {
		SpringApplication.run(MarkoneApplication.class, args);
	}

}

